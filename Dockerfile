ARG PARENT_IMAGE
FROM $PARENT_IMAGE

ARG DOCKER_VERSION
ENV DOCKER_VERSION=$DOCKER_VERSION

ARG DOCKER_BUILDX_VERSION
ENV DOCKER_BUILDX_VERSION=$DOCKER_BUILDX_VERSION

# Install Docker Engine
ADD https://download.docker.com/linux/static/stable/aarch64/docker-$DOCKER_VERSION.tgz /
ADD https://github.com/docker/buildx/releases/download/v${DOCKER_BUILDX_VERSION}/buildx-v${DOCKER_BUILDX_VERSION}.linux-arm64 /docker-buildx

RUN \
    set -eux && \
    apt update && \
    apt install --no-install-recommends -y git wget curl unzip ca-certificates && \
    apt -qq -y -o Dpkg::Options::="--force-confold" full-upgrade && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
    tar -xzf docker-$DOCKER_VERSION.tgz && \
    rm -rf docker-$DOCKER_VERSION.tgz && \
    cp docker/* /usr/bin/ && \
    chmod +x /usr/bin/docker* && \
    rm -rf ./docker && \
    docker --version 

Run \
    set -eux && \
    cd / && \
	plugin='/usr/local/libexec/docker/cli-plugins/docker-buildx' && \
	mkdir -p "$(dirname "$plugin")" && \
	mv -vT 'docker-buildx' "$plugin" && \
	chmod +x "$plugin" && \
	docker buildx version


# Install latest AWS CLI v2
RUN \
    curl "https://awscli.amazonaws.com/awscli-exe-linux-aarch64.zip" -o "awscliv2.zip" && \
    unzip -qq awscliv2.zip && \
    rm -rf ./awscliv2.zip && \
    ./aws/install && \
    aws --version